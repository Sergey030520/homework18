#include <iostream>

template <typename type>
class Stack {
private:
	int countEl;
	int MaxElement;
	type* array;
	void increase_size() { MaxElement += 5; }
	void decrease_size() { MaxElement -= 5; }
	void update_stack() {
		type* new_array = new type[MaxElement];
		for (int i = 0; i < countEl; i++) *(new_array + i) = *(array + i);
		delete[] array;
		array = new_array;
	}
	void decrease_stack(){
		if (countEl > 10) {
			decrease_size();
			update_stack();
		}
	}
	void increase_stack(){
		increase_size(); 
		update_stack();
	}
public:
	Stack() {
		countEl = 1, MaxElement = 4;
		array = new type[MaxElement];
	}
	void push(type value) {
		if (countEl == MaxElement) increase_stack();
		*(array + countEl) = value;
		countEl++;
	}
	void clear() { 
		delete [] array;
		countEl = 0, MaxElement = 4;
		array = new type[MaxElement];
	}
	type pop() { 
		type value = (countEl > 1 ? *(array + (--countEl)) : *(array-1));
		if (MaxElement - countEl == 10) 
			decrease_stack();
		return value;
	}
	type top() { return *(array + countEl - 1); }
};

int main() {
	Stack<double>* stack = new Stack<double>();
	for (int i = 0; i < 40; ++i) stack->push(i + 1.1234);
	for (int i = 0; i < 20; ++i) std::cout << "Value:" << stack->pop() << std::endl;
	
	Stack<char>* stack_ch = new Stack<char>();
	char text[] = "hello, guys and nice to meet you!";
	for (int i = 0; *(text + i) != '\0'; ++i) stack_ch->push(*(text + i));
	for (int i = 0; i < 40; ++i) std::cout << "Value:" << stack_ch->pop() << std::endl;
	stack_ch->clear();
}